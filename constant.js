const ROLE = {
  STUDENT: 'นิสิต',
  DEAN: 'คณบดี',
  PROFESSOR: 'อาจารย์',
  SYSTEM_ADMIN: 'แอดมินระบบ',
  INSTITUTE_ADMIN: 'แอดมินหน่วยงาน',
  APPROVER: 'ผู้พิจารณา'
}

const ROOMRESERVATION_DATA = {
  WAITING: 'รออนุมัติ',
  DISAPPROVE: 'ไม่อนุมัติ',
  APPROVE: 'อนุมัติ'
}

const TYPEROOM = {
  BIG_ROOM: 'ห้องประชุมใหญ่', // max 300
  MIDDLE_ROOM: 'ห้องประชุมกลาง', // max 200
  SMALL_ROOM: 'ห้องประชุมเล็ก' // max 100
}

const ITEMINROOM = {
  CHAIR: 'เก้าอี้',
  TABLE: 'โต๊ะ',
  PRESENTER: 'รีโมทนำเสนอ',
  TV: 'โทรทัศน์',
  OHP: 'เครื่องฉายภาพบนผนัง',
  PROJECTOR: 'เครื่องฉายภาพ',
  COMPUTER: 'คอมพิวเตอร์'
}

module.exports = {
  ROLE,
  ROOMRESERVATION_DATA,
  TYPEROOM,
  ITEMINROOM
}
