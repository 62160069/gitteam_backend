const express = require('express')
const router = express.Router()
const Building = require('../Schema/Building')

const getAllBuildings = async function (req, res, next) {
  try {
    const allBuildings = await Building.find({}).populate({ path: 'rooms' }).exec()
    res.status(200).json(allBuildings)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const getBuildingByInstitude = async function (req, res, next) {
  const ObjectId = require('mongoose').Types.ObjectId
  try {
    const building = await Building.find({ institute: new ObjectId(req.params.id) }).populate({ path: 'rooms' }).populate({ path: 'institute' })
    console.log('building ' + building)
    if (building === null) {
      return res.status(404).json({
        message: 'Building not found!!!'
      })
    }
    res.json(building)
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
}
const getBuilding = async function (req, res, next) {
  try {
    const building = await Building.findById(req.params.id).exec()
    if (building === null) {
      return res.status(404).json({
        message: 'Building not found!!!'
      })
    }
    res.json(building)
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
}
const addBuilding = async function (req, res, next) {
  const newBuilding = new Building({
    name: req.body.name,
    rooms: req.body.rooms,
    imageBuilding: req.body.imageBuilding,
    institute: req.body.institute
  })
  try {
    await newBuilding.save()
    res.status(201).json(newBuilding)
  } catch (err) {
    return res.status(500).send({
      massage: err.massage
    })
  }
}
const updateBuilding = async function (req, res, next) {
  const buildingId = req.params.id
  try {
    const building = await Building.findById(buildingId)
    building.name = req.body.name
    building.rooms = req.body.rooms
    building.imageBuilding = req.body.imageBuilding
    building.institute = req.body.institute
    await building.save()
    return res.status(200).json(building)
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

const deleteBuilding = async function (req, res, next) {
  const buildingId = req.params.id
  try {
    await Building.findByIdAndDelete(buildingId)
    return res.status(200).send('Delete building : ' + buildingId)
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

router.get('/', getAllBuildings)
router.get('/:id', getBuilding)

router.get('/a/:id', getBuildingByInstitude)

router.post('/', addBuilding)
router.put('/:id', updateBuilding)
router.delete('/:id', deleteBuilding)

module.exports = router
