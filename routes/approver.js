const express = require('express')
const router = express.Router()
const Approver = require('../Schema/Approver')
const User = require('../Schema/User')

const getAllApproverList = async function (req, res, next) {
  try {
    const allApprover = await Approver.find({}).exec()
    console.log('All approvers : ' + allApprover.length + ' people(s).')
    res.status(200).json(allApprover)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const getApproverByList = async function (req, res, next) {
  try {
    const approver = await Approver.findById(req.params.id).exec()
    console.log('Approvers : ' + approver + ' people(s).')
    if (approver === null) {
      return res.status(404).json({
        message: 'Approver not found!!!'
      })
    }
    res.status(200).json(approver)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const getApproverUser = async function (req, res, next) {
  try {
    const userApprover = await User.findById(req.params.id).exec()
    const approver = await Approver.find(userApprover).populate('listApprover').exec()
    console.log('Approver : ' + userApprover + ' people.')
    console.log('approver : ' + approver)
    if (approver === null) {
      return res.status(404).json({
        message: 'Approver not found!!!'
      })
    }
    res.status(200).json(userApprover)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}
const addApprover = async function (req, res, next) {
  const newApprover = new Approver({
    name: req.body.name,
    list: req.body.listApprover,
    institute: req.body.institute
  })
  try {
    await newApprover.save()
    res.status(201).json(newApprover)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const updateApprover = async function (req, res, next) {
  try {
    const approver = await Approver.findById(req.params.id).exec()
    console.log('approver id : ' + approver)
    approver.name = req.body.name
    approver.listApprover = req.body.listApprover
    approver.institute = req.body.institute
    await approver.save()
    return res.status(200).json(approver)
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
}

const deleteApprover = async function (req, res, next) {
  try {
    await Approver.findByIdAndDelete(req.params.id)
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

router.get('/', getAllApproverList) // ค้นหา list approver ทั้งหมด
router.get('/:id', getApproverByList) // ค้นหา list approver จาก id list
router.get('/user/:id', getApproverUser) // คนใน list approver
router.post('/', addApprover)
router.put('/:id', updateApprover)
router.delete('/:id', deleteApprover)

module.exports = router
