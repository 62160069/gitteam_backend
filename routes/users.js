const express = require('express')
const User = require('../Schema/User')
const router = express.Router()

// GET All USER
const getAllUsers = async function (req, res, next) {
  try {
    const allUsers = await User.find({})
    res.status(200).json(allUsers)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}
const getAllUsersRoleByInstitute = async function (req, res, next) {
  const ObjectId = require('mongoose').Types.ObjectId
  try {
    const allUsers = await User.find({ institute: ObjectId(req.params.id) })
    res.status(200).json(allUsers)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

// GET USER byID
const getUser = async function (req, res, next) {
  try {
    const user = await User.findById(req.params.id).exec()
    if (user === null) {
      return res.status(404).json({
        message: 'User not found!!!'
      })
    }
    res.json(user)
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
}

// Add User
const addUser = async function (req, res, next) {
  const newUser = new User({
    name: req.body.name,
    surname: req.body.surname,
    username: req.body.username,
    password: req.body.password,
    role: req.body.role
  })
  try {
    await newUser.save()
    res.status(201).json(newUser)
  } catch (err) {
    return res.status(500).send({
      massage: err.massage
    })
  }
}

// Update User
const updateUser = async function (req, res, next) {
  const userId = req.params.id
  try {
    const user = await User.findById(userId)
    user.name = req.body.name
    user.surname = req.body.surname
    user.username = req.body.username
    user.password = req.body.password
    user.role = req.body.role
    await user.save()
    return res.status(200).json(user)
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

// Delete User
const deleteUser = async function (req, res, next) {
  const userId = req.params.id
  try {
    await User.findByIdAndDelete(userId)
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

router.get('/', getAllUsers)
router.get('/Role/:id', getAllUsersRoleByInstitute)
router.get('/:id', getUser)
router.post('/', addUser)
router.put('/:id', updateUser)
router.delete('/:id', deleteUser)

module.exports = router
