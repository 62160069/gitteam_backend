const express = require('express')
const router = express.Router()
const Institute = require('../Schema/Institute')

// GET All Institute
const getAllInstitute = async function (req, res, next) {
  try {
    const allInstitute = await Institute.find({})
    res.status(200).json(allInstitute)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

// GET Institute byID
const getInstitute = async function (req, res, next) {
  try {
    const institute = await Institute.findById(req.params.id).exec()
    if (institute === null) {
      return res.status(404).json({
        message: 'Institute not found!!!'
      })
    }
    res.json(institute)
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
}

// Add Institute
const addInstitute = async function (req, res, next) {
  const newInstitute = new Institute({
    name: req.body.name,
    user: req.body.user
  })
  try {
    await newInstitute.save()
    res.status(201).json(newInstitute)
  } catch (err) {
    return res.status(500).send({
      massage: err.massage
    })
  }
}

// Update Institute
const updateInstitute = async function (req, res, next) {
  const InstituteId = req.params.id
  try {
    const institute = await Institute.findById(InstituteId)
    institute.name = req.body.name
    institute.user = req.body.user

    await institute.save()
    return res.status(200).json(institute)
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

// Delete Institute
const deleteInstitute = async function (req, res, next) {
  const instituteId = req.params.id
  try {
    await Institute.findByIdAndDelete(instituteId)
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}
router.get('/', getAllInstitute)
router.get('/:id', getInstitute)
router.post('/', addInstitute)
router.put('/:id', updateInstitute)
router.delete('/:id', deleteInstitute)

module.exports = router
