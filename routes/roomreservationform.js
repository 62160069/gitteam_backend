const express = require('express')
const router = express.Router()
const RoomReservationForm = require('../Schema/RoomReservation_Form')

// GET All RoomReservationForms
const getAllRoomReservationForms = async function (req, res, next) {
  try {
    const allRoomReservationForms = await RoomReservationForm.find({})
    res.status(200).json(allRoomReservationForms)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

// GET RoomReservationForms by ID reservation
const getRoomReservationFormsByUser = async function (req, res, next) {
  const ObjectId = require('mongoose').Types.ObjectId
  try {
    const roomReservationForm = await RoomReservationForm.find({ user: new ObjectId(req.params.id) }).populate({ path: 'room' }).populate({ path: 'user' })
    console.log(roomReservationForm)
    if (roomReservationForm === null) {
      return res.status(404).json({
        message: 'ReservationForm not found!!!'
      })
    }
    res.json(roomReservationForm)
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
}

const addRoomReservationForms = async function (req, res, next) {
  const dateTimeStart = new Date(req.body.startDate + ' ' + req.body.startTime)
  const dateTimeEnd = new Date(req.body.endDate + ' ' + req.body.endTime)

  const newRoomReservationForm = new RoomReservationForm({
    room: req.body.RoomId,
    user: req.body.IDUser,
    startDate: dateTimeStart,
    endDate: dateTimeEnd,
    item: req.body.selected,
    approverList: req.body.approverList
  })
  try {
    newRoomReservationForm.save()
    res.status(201).json(newRoomReservationForm)
  } catch (err) {
    return res.status(500).send({
      massage: err.massage
    })
  }
}

const deleteRoomReservationForm = async function (req, res, next) {
  const roomFoom = req.params.id
  try {
    await RoomReservationForm.findByIdAndDelete(roomFoom)
    return res.status(200).send('Delete RoomReservationForm : ' + roomFoom)
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

router.get('/', getAllRoomReservationForms)
router.get('/user/:id', getRoomReservationFormsByUser)
router.post('/', addRoomReservationForms)
router.delete('/:id', deleteRoomReservationForm)

module.exports = router
