const express = require('express')
const router = express.Router()
const Room = require('../Schema/Room')

const getAllRoom = async function (req, res, next) {
  try {
    const allRooms = await Room.find({}).exec()
    res.status(200).json(allRooms)
    console.log('All Room : ' + allRooms.length + 'Rooms.')
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const getRooms = async function (req, res, next) {
  try {
    const rooms = await Room.findById(req.params.id).populate({ path: 'building' }).exec()
    if (rooms === null) {
      return res.status(404).json({
        message: 'Room not found!!!'
      })
    }
    res.json(rooms)
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
}

const addRoom = async function (req, res, next) {
  const newRoom = new Room({
    name: req.body.name,
    capacity: req.body.capacity,
    floor: req.body.floor,
    building: req.body.building,
    imageRoom: req.body.imageRoom,
    Approver: req.body.Approver,
    typeRoom: req.body.typeRoom
  })
  try {
    await newRoom.save()
    res.status(201).json(newRoom)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const updateRoom = async function (req, res, next) {
  try {
    const room = await Room.findById(req.params.id)
    room.name = req.body.name
    room.capacity = req.body.capacity
    room.floor = req.body.floor
    room.building = req.body.building
    room.imageRoom = req.body.imageRoom
    room.Approver = req.body.Approver
    room.typeRoom = req.body.typeRoom
    await room.save()
    return res.status(200).json(room)
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

const deleteRoom = async function (req, res, next) {
  try {
    await Room.findByIdAndDelete(req.params.id)
    return res.status(200).send('Delete room : ' + req.params.id)
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

router.get('/', getAllRoom)
router.get('/:id', getRooms)
router.post('/', addRoom)
router.put('/:id', updateRoom)
router.delete('/:id', deleteRoom)

module.exports = router
