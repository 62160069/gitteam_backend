const express = require('express')
const ApproverReservation = require('../Schema/ApproverReservation')
const router = express.Router()

// GET All ApproverReservation
const getAllApproverReservations = async function (req, res, next) {
  try {
    const allApproverReservations = await ApproverReservation.find({})
    res.status(200).json(allApproverReservations)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

// GET ApproverReservations byID
const getApproverReservations = async function (req, res, next) {
  try {
    const approverReservations = await ApproverReservation.findById(req.params.id).exec()
    if (approverReservations === null) {
      return res.status(404).json({
        message: 'ApproverReservations not found!!!'
      })
    }
    res.json(approverReservations)
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
}

// Add ApproverReservation
const addApproverReservation = async function (req, res, next) {
  const newApproverReservation = new ApproverReservation({
    user: req.body.user,
    dateApprove: req.body.dateApprove,
    statusApporver: req.body.statusApporver,
    ApproverIndex: req.body.ApproverIndex

  })
  try {
    await newApproverReservation.save()
    res.status(201).json(newApproverReservation)
  } catch (err) {
    return res.status(500).send({
      massage: err.massage
    })
  }
}

// Update ApproverReservation
const updateApproverReservation = async function (req, res, next) {
  const ApproverReservationId = req.params.id
  try {
    const approverReservation = await ApproverReservation.findById(ApproverReservationId)
    approverReservation.user = req.body.user
    approverReservation.dateApprove = req.body.dateApprove
    approverReservation.statusApporver = req.body.statusApporver
    approverReservation.ApproverIndex = req.body.ApproverIndex
    await approverReservation.save()
    return res.status(200).json(approverReservation)
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

// Delete ApproverReservation
const deleteApproverReservation = async function (req, res, next) {
  const approverReservationId = req.params.id
  try {
    await ApproverReservation.findByIdAndDelete(approverReservationId)
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

router.get('/', getAllApproverReservations)
router.get('/:id', getApproverReservations)
router.post('/', addApproverReservation)
router.put('/:id', updateApproverReservation)
router.delete('/:id', deleteApproverReservation)

module.exports = router
