const mongoose = require('mongoose')
const { Schema } = mongoose
const bcrypt = require('bcryptjs')

const userSchema = new Schema({
  name: { type: String },
  surname: { type: String },
  username: { type: String },
  password: { type: String },
  role: {
    type: [String],
    default: []
  }, // อาจมีการเปลี่ยนแปลง
  institute: [{ type: Schema.Types.ObjectId, ref: 'Institute', default: [] }] // หน่วยงาน
})
userSchema.pre('save', function (next) {
  const user = this

  if (this.isModified('password') || this.isNew) {
    bcrypt.genSalt(10, function (saltError, salt) {
      if (saltError) {
        return next(saltError)
      } else {
        bcrypt.hash(user.password, salt, function (hashError, hash) {
          if (hashError) {
            return next(hashError)
          }

          user.password = hash
          next()
        })
      }
    })
  } else {
    return next()
  }
})
module.exports = mongoose.model('User', userSchema)
