const mongoose = require('mongoose')
const { Schema } = mongoose
const ApproverReservationSchema = new Schema({
  user: { type: Schema.Types.ObjectId, ref: 'User' },
  dateApprove: Date,
  statusApporver: { type: String, default: '' }, // อนุมัติ
  ApproverIndex: { type: Number, default: '' } // ใครกำลังอนุมัติ
})
module.exports = mongoose.model('ApproverReservation', ApproverReservationSchema)
