const mongoose = require('mongoose')
const { Schema } = mongoose
const roomSchema = Schema({
  name: { type: String },
  capacity: { type: Number },
  floor: Number,
  building: [{ type: Schema.Types.ObjectId, ref: 'Building' }],
  imageRoom: { type: String }, // ภาพห้อง
  typeRoom: [{ type: String }],
  Approver: [{ type: Schema.Types.ObjectId, ref: 'Approver' }]
})
module.exports = mongoose.model('Room', roomSchema)
