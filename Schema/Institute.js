const mongoose = require('mongoose')
const { Schema } = mongoose
const instituteSchema = new Schema({
  name: { type: String },
  user: [{ type: Schema.Types.ObjectId, ref: 'User', default: [] }]
})
module.exports = mongoose.model('Institute', instituteSchema)
