const mongoose = require('mongoose')
const { Schema } = mongoose
const approverSchema = new Schema({
  name: { type: String },
  listApprover: [{ type: Schema.Types.ObjectId, ref: 'User', default: [] }],
  institute: { type: Schema.Types.ObjectId, ref: 'Institute' } // หน่วยงาน
})
module.exports = mongoose.model('Approver', approverSchema)
