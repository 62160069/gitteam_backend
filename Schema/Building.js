const mongoose = require('mongoose')
const { Schema } = mongoose
const buildingSchema = new Schema({
  name: { type: String }, // ชื่อตึก
  rooms: [{ type: Schema.Types.ObjectId, ref: 'Room', default: [] }], // ห้อง
  imageBuilding: { type: String }, // ภาพตึก
  institute: { type: Schema.Types.ObjectId, ref: 'Institute' } // หน่วยงาน
})
module.exports = mongoose.model('Building', buildingSchema)
