const mongoose = require('mongoose')
const { Schema } = mongoose
const roomReservationSchema = new Schema({
  room: { type: Schema.Types.ObjectId, ref: 'Room' },
  user: [{ type: Schema.Types.ObjectId, ref: 'User' }],
  startDate: Date,
  endDate: Date,
  item: [{ type: String }],
  approverList: [{ type: Schema.Types.ObjectId, ref: 'ApproverReservation' }]
})
module.exports = mongoose.model('RoomReservation_From', roomReservationSchema)
