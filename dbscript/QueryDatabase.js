const mongoose = require('mongoose')
const { ROLE, TYPEROOM, ITEMINROOM, ROOMRESERVATION_DATA } = require('../constant.js')
const User = require('../Schema/User.js')
const Building = require('../Schema/Building')
const Room = require('../Schema/Room')
const Approver = require('../Schema/Approver')
const RoomReservationForm = require('../Schema/RoomReservation_Form.js')
const ApproverReservation = require('../Schema/ApproverReservation.js')
const Institute = require('../Schema/Institute.js')
mongoose.connect('mongodb://localhost/BOOKINGBUU')

async function clearData () {
  await Approver.deleteMany({})
  await Institute.deleteMany({})
  await User.deleteMany({})
  await Building.deleteMany({})
  await Room.deleteMany({})
  await RoomReservationForm.deleteMany({})
  await ApproverReservation.deleteMany({})
}
// Room Reservation Form ,User ,Institute By Fon
// Approver ,Building ,Room By Bm
async function main () {
  await clearData()
  const IF = new Institute({ name: 'คณะวิทยการการสารสนเทศ' })
  const FE = new Institute({ name: 'คณะวิทยาศาสตร์การอาหาร' })
  const CI = new Institute({ name: 'อาคารวิศวกรรมภาคอุตสาหการ' })
  const ME = new Institute({ name: 'อาคารวิศวกรรมภาคเครื่องกล' })
  const IC = new Institute({ name: 'วิทยาลัยนานาชาติ' })
  const BUU = new Institute({ name: 'มหาวิทยาลับบูรพา' })
  // All user student
  const user1 = new User({ name: 'นุชจรีย์', surname: 'แสงรัตน์', username: '62160137@go.buu.ac.th', password: 'as123', role: [ROLE.STUDENT], institute: IF })
  const user2 = new User({ name: 'วัชรพล', surname: 'จันทร์บุญ', username: '62160069@go.buu.ac.th', password: 'as123', role: [ROLE.STUDENT], institute: FE })
  const user3 = new User({ name: 'เรณู', surname: 'ผาระกัน', username: '62160141@go.buu.ac.th', password: 'as123', role: [ROLE.STUDENT], institute: CI })
  const user4 = new User({ name: 'นิธิภัทร', surname: 'ลิขิตวัฒนกิจ', username: '62160281@go.buu.ac.th', password: 'as123', role: [ROLE.STUDENT], institute: ME })
  const user5 = new User({ name: 'สุพิชญา', surname: 'ตราสี', username: '62160312@go.buu.ac.th', password: 'as123', role: [ROLE.STUDENT], institute: IC })
  // All user professor IF
  const user6 = new User({ name: 'วรวิทย์', surname: 'วีรพันธุ์', username: 'werapan@go.buu.ac.th', password: 'as123', role: [ROLE.PROFESSOR, ROLE.APPROVER], institute: IF })
  const user7 = new User({ name: 'จักริน', surname: 'สุขสวัสดิ์ชน', username: 'jakkalin@go.buu.ac.th', password: 'as123', role: [ROLE.PROFESSOR, ROLE.APPROVER], institute: IF })
  const user8 = new User({ name: 'กฤษณะ', surname: 'ชินสาร', username: 'kisana@go.buu.ac.th', password: 'as123', role: [ROLE.PROFESSOR, ROLE.APPROVER], institute: IF })
  // All user professor FE
  const user9 = new User({ name: 'กิดาการ', surname: 'สายธนู', username: 'ksaithan@go.buu.ac.th', password: 'as123', role: [ROLE.PROFESSOR, ROLE.APPROVER], institute: FE })
  const user10 = new User({ name: 'อภิสิทธิ์', surname: 'ภคพงศ์พันธุ์', username: 'apisit@go.buu.ac.th', password: 'as123', role: [ROLE.PROFESSOR, ROLE.APPROVER], institute: FE })
  const user11 = new User({ name: 'คณินทร์', surname: 'ธีรภาพโอฬาร', username: 'kanint@go.buu.ac.th', password: 'as123', role: [ROLE.PROFESSOR, ROLE.APPROVER], institute: FE })
  // All user professor CI
  const user12 = new User({ name: 'จตุภัทร', surname: 'เมฆพายัพ', username: 'jatupat@go.buu.ac.th', password: 'as123', role: [ROLE.PROFESSOR, ROLE.APPROVER], institute: CI })
  const user13 = new User({ name: 'จุฑาพร', surname: 'เนียมวงษ์', username: 'jutaporn@go.buu.ac.th', password: 'as123', role: [ROLE.PROFESSOR, ROLE.APPROVER], institute: CI })
  const user14 = new User({ name: 'ชาติไทย', surname: 'ไทยประยูร', username: 'chatthai@go.buu.ac.th', password: 'as123', role: [ROLE.PROFESSOR, ROLE.APPROVER], institute: CI })
  // All user professor ME
  const user15 = new User({ name: 'ดวงกมล', surname: 'ดวงกมล', username: 'duangkamolp@go.buu.ac.th', password: 'as123', role: [ROLE.PROFESSOR, ROLE.APPROVER], institute: ME })
  const user16 = new User({ name: 'เดชชาติ', surname: 'สามารถ', username: 'detchat.sa@go.buu.ac.th', password: 'as123', role: [ROLE.PROFESSOR, ROLE.APPROVER], institute: ME })
  const user17 = new User({ name: 'บำรุงศักดิ์', surname: 'เผื่อนอารีย์', username: 'bumrungsak@go.buu.ac.th', password: 'as123', role: [ROLE.PROFESSOR, ROLE.APPROVER], institute: ME })
  // All user professor IC
  const user18 = new User({ name: 'ปรียารัตน์', surname: 'นาคสุวรรณ์', username: 'preeyarat@go.buu.ac.th', password: 'as123', role: [ROLE.PROFESSOR, ROLE.APPROVER], institute: IC })
  const user19 = new User({ name: 'พัชรี วงษ์เกษม', surname: 'วงษ์เกษม', username: 'wongkasem@go.buu.ac.th', password: 'as123', role: [ROLE.PROFESSOR, ROLE.APPROVER], institute: IC })
  const user20 = new User({ name: 'รักพร', surname: 'ดอกจันทร์', username: 'rakporn@go.buu.ac.th', password: 'as123', role: [ROLE.PROFESSOR, ROLE.APPROVER], institute: IC })
  // All user admin system
  const user21 = new User({ name: 'วนิดา', surname: 'พงษ์ศักดิ์ชาติ', username: 'vanida@go.buu.ac.th', password: 'as123', role: [ROLE.SYSTEM_ADMIN], institute: BUU })
  // All user admin institute
  const user22 = new User({ name: 'วรวิมล', surname: 'เจริญทัมมะสถิต', username: 'ank.buu@go.buu.ac.th', password: 'as123', role: [ROLE.INSTITUTE_ADMIN], institute: IF })
  const user23 = new User({ name: 'วริน', surname: 'วิพิศมากูล', username: 'wasinv@go.buu.ac.th', password: 'as123', role: [ROLE.INSTITUTE_ADMIN], institute: FE })
  const user24 = new User({ name: 'สมคิด', surname: 'อินเทพ', username: 'intep@go.buu.ac.th', password: 'as123', role: [ROLE.INSTITUTE_ADMIN], institute: CI })
  const user25 = new User({ name: 'สหัทยา', surname: 'รัตนะมงคลกุล', username: 'sahattay@go.buu.ac.th', password: 'as123', role: [ROLE.INSTITUTE_ADMIN], institute: ME })
  const user26 = new User({ name: 'สาธินี', surname: 'เลิศประไพ', username: 'satineel@go.buu.ac.th', password: 'as123', role: [ROLE.INSTITUTE_ADMIN], institute: IC })
  // List Approver IF
  const listApproverIF1 = new Approver({ name: 'List Approver 1', listApprover: [user6, user7, user8], institute: IF })
  const listApproverIF2 = new Approver({ name: 'List Approver 2', listApprover: [user7, user8], institute: IF })
  // List Approver FE
  const listApproverFE1 = new Approver({ name: 'List Approver 1', listApprover: [user9, user10, user11], institute: FE })
  const listApproverFE2 = new Approver({ name: 'List Approver 2', listApprover: [user10, user11], institute: FE })
  // List Approver CI
  const listApproverCI1 = new Approver({ name: 'List Approver 1', listApprover: [user12, user13, user14], institute: CI })
  // List Approver ME
  const listApproverME1 = new Approver({ name: 'List Approver 1', listApprover: [user15, user16, user17], institute: ME })
  // List Approver IC
  const listApproverIC1 = new Approver({ name: 'List Approver 1', listApprover: [user18, user19, user20], institute: IC })
  const listApproverIC2 = new Approver({ name: 'List Approver 1', listApprover: [user18, user19], institute: IC })
  listApproverIF1.save()
  listApproverIF2.save()
  listApproverIC1.save()
  listApproverIC2.save()
  listApproverME1.save()
  listApproverCI1.save()
  listApproverFE1.save()
  listApproverFE2.save()
  // All Building
  const BuildingIF = new Building({ name: 'คณะวิทยการการสารสนเทศ', imageBuilding: 'IF.png', institute: IF })
  const BuildingFE = new Building({ name: 'คณะวิทยาศาสตร์การอาหาร', imageBuilding: 'FE.png', institute: FE })
  const BuildingCI = new Building({ name: 'อาคารวิศวกรรมภาคอุตสาหการ', imageBuilding: 'CI.png', institute: CI })
  const BuildingME = new Building({ name: 'อาคารวิศวกรรมภาคเครื่องกล', imageBuilding: 'ME.png', institute: ME })
  const BuildingIC = new Building({ name: 'วิทยาลัยนานาชาติ', imageBuilding: 'IC.png', institute: IC })
  // All Room IF
  const IF3C01 = new Room({ name: 'IF-3C01', capacity: 60, floor: 3, building: BuildingIF, imageRoom: 'IF3C01.png', typeRoom: [TYPEROOM.SMALL_ROOM], Approver: listApproverIF2 })
  const IF11M280 = new Room({ name: 'IF-11M280', capacity: 280, floor: 11, building: BuildingIF, imageRoom: 'IF11M280.png', typeRoom: [TYPEROOM.BIG_ROOM], Approver: listApproverIF1 })
  const IF4C01 = new Room({ name: 'IF-4C01', capacity: 60, floor: 4, building: BuildingIF, imageRoom: 'IF4C01.png', typeRoom: [TYPEROOM.SMALL_ROOM], Approver: listApproverIF2 })
  const IF7T05 = new Room({ name: 'IF-7T05', capacity: 150, floor: 7, building: BuildingIF, imageRoom: 'IF7T05.png', typeRoom: [TYPEROOM.MIDDLE_ROOM], Approver: listApproverIF2 })
  const IF5M210 = new Room({ name: 'IF-5M210', capacity: 210, floor: 5, building: BuildingIF, imageRoom: 'IF5M210.png', typeRoom: [TYPEROOM.BIG_ROOM], Approver: listApproverIF2 })
  // Add RoomIF To BuildingIF
  BuildingIF.rooms.push(IF3C01)
  BuildingIF.rooms.push(IF11M280)
  BuildingIF.rooms.push(IF4C01)
  BuildingIF.rooms.push(IF7T05)
  BuildingIF.rooms.push(IF5M210)
  // All Room FR
  const FE101 = new Room({ name: 'FE-101', capacity: 20, floor: 1, building: BuildingFE, imageRoom: 'FE101.png', typeRoom: [TYPEROOM.SMALL_ROOM], Approver: listApproverFE1 })
  const FE102 = new Room({ name: 'FE-102', capacity: 80, floor: 1, building: BuildingFE, imageRoom: 'FE102.png', typeRoom: [TYPEROOM.SMALL_ROOM], Approver: listApproverFE2 })
  const FE104 = new Room({ name: 'FE-104', capacity: 10, floor: 1, building: BuildingFE, imageRoom: 'FE104.png', typeRoom: [TYPEROOM.SMALL_ROOM], Approver: listApproverFE2 })
  const FE215 = new Room({ name: 'FE-215', capacity: 30, floor: 2, building: BuildingFE, imageRoom: 'FE215.png', typeRoom: [TYPEROOM.SMALL_ROOM], Approver: listApproverFE2 })
  const FE201 = new Room({ name: 'FE-201', capacity: 10, floor: 2, building: BuildingFE, imageRoom: 'FE201.png', typeRoom: [TYPEROOM.SMALL_ROOM], Approver: listApproverFE2 })
  // Add RoomFE To BuildingFE
  BuildingFE.rooms.push(FE101)
  BuildingFE.rooms.push(FE102)
  BuildingFE.rooms.push(FE104)
  BuildingFE.rooms.push(FE215)
  BuildingFE.rooms.push(FE201)
  // All Room CI
  const CI300 = new Room({ name: 'CI-300', capacity: 300, floor: 1, building: BuildingCI, imageRoom: 'CI300.png', typeRoom: [TYPEROOM.BIG_ROOM], Approver: listApproverCI1 })
  // Add RoomCI To BuildingCI
  BuildingCI.rooms.push(CI300)
  // All Room ME
  const ME101 = new Room({ name: 'ME-101', capacity: 30, floor: 1, building: BuildingME, imageRoom: 'ME101.png', typeRoom: [TYPEROOM.SMALL_ROOM], Approver: listApproverME1 })
  const ME105 = new Room({ name: 'ME-105', capacity: 60, floor: 1, building: BuildingME, imageRoom: 'ME105.png', typeRoom: [TYPEROOM.SMALL_ROOM], Approver: listApproverME1 })
  const ME202 = new Room({ name: 'ME-202', capacity: 60, floor: 2, building: BuildingME, imageRoom: 'ME202.png', typeRoom: [TYPEROOM.SMALL_ROOM], Approver: listApproverME1 })
  const ME203 = new Room({ name: 'ME-203', capacity: 60, floor: 2, building: BuildingME, imageRoom: 'ME203.png', typeRoom: [TYPEROOM.SMALL_ROOM], Approver: listApproverME1 })
  const ME112 = new Room({ name: 'ME-112', capacity: 40, floor: 1, building: BuildingME, imageRoom: 'ME112.png', typeRoom: [TYPEROOM.SMALL_ROOM], Approver: listApproverME1 })
  // Add RoomME To BuildingME
  BuildingME.rooms.push(ME101)
  BuildingME.rooms.push(ME105)
  BuildingME.rooms.push(ME202)
  BuildingME.rooms.push(ME203)
  BuildingME.rooms.push(ME112)
  // All Room IC
  const IC111 = new Room({ name: 'IC-111', capacity: 50, floor: 1, building: BuildingIC, imageRoom: 'IC111.png', typeRoom: [TYPEROOM.SMALL_ROOM], Approver: listApproverIC1 })
  const IC203 = new Room({ name: 'IC-203', capacity: 200, floor: 2, building: BuildingIC, imageRoom: 'IC203.png', typeRoom: [TYPEROOM.MIDDLE_ROOM], Approver: listApproverIC2 })
  const IC210 = new Room({ name: 'IC-210', capacity: 120, floor: 2, building: BuildingIC, imageRoom: 'IC210.png', typeRoom: [TYPEROOM.MIDDLE_ROOM], Approver: listApproverIC2 })
  const IC306 = new Room({ name: 'IC-306', capacity: 50, floor: 3, building: BuildingIC, imageRoom: 'IC306.png', typeRoom: [TYPEROOM.MIDDLE_ROOM], Approver: listApproverIC2 })
  const IC401 = new Room({ name: 'IC-401', capacity: 50, floor: 4, building: BuildingIC, imageRoom: 'IC401.png', typeRoom: [TYPEROOM.MIDDLE_ROOM], Approver: listApproverIC2 })
  // Add RoomIC To BuildingIC
  BuildingIC.rooms.push(IC111)
  BuildingIC.rooms.push(IC203)
  BuildingIC.rooms.push(IC210)
  BuildingIC.rooms.push(IC306)
  BuildingIC.rooms.push(IC401)

  const ApproverRes1 = new ApproverReservation({
    user: listApproverIF1.listApprover[0],
    dateApprove: new Date('2022-04-2 12:00'),
    statusApporver: ROOMRESERVATION_DATA.APPROVE, // อนุมัติ
    ApproverIndex: 1 // ใครกำลังอนุมัติ
  })

  const ApproverRes2 = new ApproverReservation({
    user: listApproverIF1.listApprover[0],
    dateApprove: new Date('2022-04-2 12:00'),
    statusApporver: ROOMRESERVATION_DATA.APPROVE, // อนุมัติ
    ApproverIndex: 2 // ใครกำลังอนุมัติ
  })
  ApproverRes1.save()
  ApproverRes2.save()

  // Add Room Reservation Form
  const Form1 = new RoomReservationForm({
    room: IF11M280,
    user: user1,
    startDate: new Date('2022-04-1 13:00'),
    endDate: new Date('2022-04-1 15:00'),
    item: [ITEMINROOM.PROJECTOR, ITEMINROOM.TV, ITEMINROOM.COMPUTER],
    approverList: [
      ApproverRes1,
      ApproverRes2
    ]
  })

  // const Form2 = new RoomReservationForm({
  //   room: FE201,
  //   user: user2,
  //   startDate: new Date('2022-04-1 13:00'),
  //   endDate: new Date('2022-04-1 15:00'),
  //   item: [ITEMINROOM.PROJECTOR, ITEMINROOM.TV, ITEMINROOM.COMPUTER],
  //   appprover: [{
  //     user: listApproverFE2.listApprover[0],
  //     dateApprove: null,
  //     status: ROOMRESERVATION_DATA.WAITING
  //   }, {
  //     user: listApproverFE2.listApprover[1],
  //     dateApprove: null,
  //     status: ROOMRESERVATION_DATA.WAITING
  //   }]
  // })
  // const Form3 = new RoomReservationForm({
  //   room: CI300,
  //   user: user3,
  //   startDate: new Date('2022-04-5 09:00'),
  //   endDate: new Date('2022-04-5 11:00'),
  //   item: [ITEMINROOM.PROJECTOR, ITEMINROOM.TV, ITEMINROOM.COMPUTER],
  //   appprover: [{
  //     user: listApproverCI1.listApprover[0],
  //     dateApprove: new Date('2022-04-6 12:00'),
  //     status: ROOMRESERVATION_DATA.APPROVE
  //   }, {
  //     user: listApproverCI1.listApprover[1],
  //     dateApprove: new Date('2022-04-6 13:00'),
  //     status: ROOMRESERVATION_DATA.APPROVE
  //   }, {
  //     user: listApproverCI1.listApprover[2],
  //     dateApprove: new Date('2022-04-6 14:00'),
  //     status: ROOMRESERVATION_DATA.DISAPPROVE
  //   }]
  // })
  // Saves
  user1.save()
  user2.save()
  user3.save()
  user4.save()
  user5.save()
  user6.save()
  user7.save()
  user8.save()
  user9.save()
  user10.save()
  user11.save()
  user12.save()
  user13.save()
  user14.save()
  user15.save()
  user16.save()
  user17.save()
  user18.save()
  user19.save()
  user20.save()
  user21.save()
  user22.save()
  user23.save()
  user24.save()
  user25.save()
  user26.save()

  IF.save()
  FE.save()
  ME.save()
  IC.save()
  BUU.save()

  BuildingIF.save()
  BuildingFE.save()
  BuildingCI.save()
  BuildingME.save()
  BuildingIC.save()

  IF7T05.save()
  IF3C01.save()
  IF11M280.save()
  IF4C01.save()
  IF5M210.save()

  FE101.save()
  FE102.save()
  FE104.save()
  FE215.save()
  FE201.save()

  IC111.save()
  IC203.save()
  IC210.save()
  IC306.save()
  IC401.save()

  ME101.save()
  ME105.save()
  ME202.save()
  ME203.save()
  ME112.save()

  CI300.save()

  Form1.save()
  // Form2.save()
  // Form3.save()
}
main().then(function () {
  console.log('Finish')
})
